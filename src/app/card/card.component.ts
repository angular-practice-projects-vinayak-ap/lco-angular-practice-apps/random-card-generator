import { Component, OnInit, Input } from '@angular/core';
import {
  faEnvelope,
  faMapMarkerAlt,
  faPhoneAlt,
  faDatabase,
  faIdCard,
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent implements OnInit {
  @Input() userCard;

  faEnvelope = faEnvelope;
  faMapMarkerAlt = faMapMarkerAlt;
  faPhoneAlt = faPhoneAlt;
  faDatabase = faDatabase;
  faIdCard = faIdCard;

  constructor() {}

  ngOnInit(): void {}
}
