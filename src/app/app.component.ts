import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { UserService } from './services/user.service';
import { faSyncAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'random-card-generator';

  user: any;
  faSyncAlt = faSyncAlt;

  constructor(
    private userService: UserService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.getRouteData();
  }

  onUpdateCard() {
    this.getRouteData();
  }

  getRouteData() {
    this.userService.getUser().subscribe(
      (data: any) => {
        console.log(data);
        this.user = data.results[0];
      },
      (err) => {
        this.toastr.error(err.status, 'Something went wrong');
      }
    );
  }
}
